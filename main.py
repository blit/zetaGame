﻿from utils import *

Balance = 0
BlitCoin = 0.0
Asic = 0
Course = 3000000  # 3 millions


def main():
    try:
        loading(0.02, Fore.BLUE)
        special_cls()
        select()
    except KeyboardInterrupt:
        pass


def select():
    cls()
    print(Fore.LIGHTRED_EX + "ZetaGame One\n")
    print(Fore.LIGHTBLUE_EX + "Гайд по игре - GUIDE\nПереход к игре - JOIN\nВыход из игры - EXIT")
    print(Fore.LIGHTCYAN_EX + "Команды можно вводить цифрами по порядку. (GUIDE - 1, JOIN - 2 и т.д.)")
    command = input(Fore.LIGHTRED_EX + "Введите команду: ")
    if command.upper() in ["GUIDE", "1"]:
        info()
    elif command.upper() in ["JOIN", "2"]:
        menu()
    elif command.upper() in ["EXIT", "3"]:
        exit()
    else:
        cls()
        colored_printing("Команда неверная! Повторите ввод.", 0.05, Fore.RED, False)
        sleep(1)
        select()


def info():
    cls()
    colored_printing("Гайд по ZetaGame One:", 0.08, Fore.LIGHTRED_EX, True)
    colored_printing("1. Управление осуществляется путём ввода команд.", 0.06, Fore.LIGHTBLUE_EX, False)
    colored_printing("2. Здесь вам нужно будет покупать майнинг-фермы (асики) и зарабатывать криптовалюту. " +
                     "Криптовалюту можно обменять на реальные деньги (рубли)." +
                     "\n3. Сразу чтоб не было вопросов, почему такие длинные команды: все команды можно вводить по номерам, по порядку. " +
                     "Например: Есть меню с пунктами A, B, C, D. Можно вместо C ввести 3, и команда сработает. НО В МЕНЮ ПОКУПКИ ФЕРМ, БУДЬТЕ ВНИМАТЕЛЬНЫ. " +
                     "\n4. В игре есть случайные события. Шанс срабатывания случайного события 1 к 25.", 0.06, Fore.LIGHTBLUE_EX, False)
    colored_printing("5. Изначально я хотел добавить сохранения, но поскольку игра проходится за полчаса, решил от них отказаться.", 0.06, Fore.LIGHTYELLOW_EX, False)
    sleep(1)
    colored_printing("6. Удачи)", 0.06, Fore.LIGHTCYAN_EX, False)
    sleep(5)
    select()


def menu():
    global Balance, BlitCoin, Asic, Course
    BlitCoin = round(BlitCoin, 8)
    Balance = round(Balance, 2)
    Course = round(Course * (random.randint(9000, 11000) / 10000), 2)  # Постепенное изменение курса
    if Course >= 6000000:
        Course -= 1000000
    elif Course <= 1000000:
        Course += 1000000

    if random.randint(1, 50) >= 49:
        event = random.randint(1, 2)
        if event == 1:
            rand_event_1()
        elif event == 2:
            rand_event_2()

    cls()
    print(Fore.LIGHTRED_EX + "ZetaGame One\n")
    print(Fore.BLUE + f"Твой баланс: {Balance} рублей.")
    print(Fore.BLUE + f"Твой криптобаланс: {BlitCoin} блиткоинов.")
    print(Fore.BLUE + f"Твоя майнинг-ферма: {Asics[Asic]}\n")

    print(Fore.LIGHTBLUE_EX + "Магазин 'ЭНВИДЕ' - SHOP")
    print(Fore.LIGHTMAGENTA_EX + "Биржа 'Bin Access' - BIN")
    print(Fore.YELLOW + "Твоя хата - MINING")
    print(Fore.LIGHTMAGENTA_EX + "Настройка музыки - MUSIC")
    print(Fore.RED + "Выход в главное меню - RETURN")
    command = input(Fore.LIGHTRED_EX + "Введите команду: ")
    if command.upper() in ["SHOP", "1"]:
        shop()
    elif command.upper() in ["BIN", "2"]:
        binance()
    elif command.upper() in ["MINING", "3"]:
        mining()
    elif command.upper() in ["MUSIC", "4"]:
        sound()
    elif command.upper() in ["RETURN", "5"]:
        select()
    else:
        cls()
        colored_printing("Команда неверная! Повторите ввод.", 0.05, Fore.RED, False)
        sleep(1)
        menu()


def shop():
    global Balance, Asic
    cls()
    print(Fore.LIGHTGREEN_EX + "Магазин 'ЭНВИДЕ'")
    print(Fore.LIGHTBLUE_EX + "Тут вы можете приобрести майнинг-фермы\n")
    print(Fore.LIGHTCYAN_EX + "Купить майнинг-ферму под номером {?} - BUY 1-10")
    print(Fore.RED + "Вернуться - RETURN\n")
    print(Fore.BLUE + "Наш ассортимент:")
    i = 0
    AS = 0
    for element in Asic_prices:
        i += 1
        AS += 1
        print(Fore.BLUE + f"{i}. {Asics[AS]}: {element} рублей.")
    command = input(Fore.LIGHTRED_EX + "Введите команду: ")
    if command.upper() in ["BUY 1", "2"]:
        if Asic > 1:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[0]:
            Asic = 1
            Balance -= Asic_prices[0]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 2", "3"]:
        if Asic > 2:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[1]:
            Asic = 2
            Balance -= Asic_prices[1]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 3", "4"]:
        if Asic > 3:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[2]:
            Asic = 3
            Balance -= Asic_prices[2]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 4", "5"]:
        if Asic > 4:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[3]:
            Asic = 4
            Balance -= Asic_prices[3]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 5", "6"]:
        if Asic > 5:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[4]:
            Asic = 5
            Balance -= Asic_prices[4]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 6", "7"]:
        if Asic > 6:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[5]:
            Asic = 6
            Balance -= Asic_prices[5]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 7", "8"]:
        if Asic > 7:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[6]:
            Asic = 7
            Balance -= Asic_prices[6]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 8", "9"]:
        if Asic > 8:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        elif Balance >= Asic_prices[7]:
            Asic = 8
            Balance -= Asic_prices[7]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 9", "10"]:
        if Asic > 9:
            cls()
            colored_printing("Ваш асик более мощный чем этот.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
        if Balance >= Asic_prices[8]:
            Asic = 9
            Balance -= Asic_prices[8]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["BUY 10", "11"]:
        if Balance >= Asic_prices[9]:
            Asic = 10
            Balance -= Asic_prices[9]
            cls()
            colored_printing("Покупка совершена.", 0.05, Fore.LIGHTGREEN_EX, False)
            sleep(1)
            shop()
            # Функция запуска финального события <- Тут
        else:
            cls()
            colored_printing("Недостаточно средств.", 0.05, Fore.RED, False)
            sleep(1)
            shop()
    elif command.upper() in ["RETURN", "1"]:
        if random.randint(1, 50) >= 49:
            rand_event_1()
        else:
            menu()
    else:
        cls()
        colored_printing("Команда неверная! Повторите ввод.", 0.05, Fore.RED, False)
        sleep(1)
        shop()


def binance():
    global BlitCoin, Balance
    cls()
    print(Fore.LIGHTMAGENTA_EX + "Биржа 'Bin Access'")
    print(Fore.LIGHTYELLOW_EX + f"Текущий курс блиткоина: {Course} рублей.")
    print(Fore.LIGHTBLUE_EX + "Тут ты можешь обменять крипту на рубли.\n")
    print(Fore.LIGHTYELLOW_EX + "Обменять блиткоины на рубли - TRADE")
    print(Fore.LIGHTCYAN_EX + "Вернуться - RETURN\n")
    command = input(Fore.LIGHTRED_EX + "Введите команду: ")
    if command.upper() in ["TRADE", "1"]:
        cls()
        Balance += trade(BlitCoin, Course)
        BlitCoin = 0.0
        sleep(2)
        binance()
    elif command.upper() in ["RETURN", "2"]:
        menu()
    else:
        cls()
        colored_printing("Команда неверная! Повторите ввод.", 0.05, Fore.RED, False)
        sleep(1)
        binance()


def mining():
    global BlitCoin, is_Final
    cls()
    print(Fore.LIGHTGREEN_EX + "Твоя хата")
    print(Fore.LIGHTMAGENTA_EX + f"Твоя майнинг-ферма: {Asics[Asic]}.\n")
    print(Fore.LIGHTBLUE_EX + "Майнить - MINE")
    print(Fore.RED + "Вернуться - RETURN")
    command = input(Fore.LIGHTRED_EX + "Введите команду: ")
    if command.upper() in ["MINE", "1"]:
        cls()
        BlitCoin += mine(Asic)
        sleep(2)
        if Asic == 10:
            final()
        elif random.randint(1, 50) >= 49 and Asic >= 2:
            rand_event_2()
        else:
            mining()
    elif command.upper() in ["RETURN", "2"]:
        menu()
    else:
        cls()
        colored_printing("Команда неверная! Повторите ввод.", 0.05, Fore.RED, False)
        sleep(1)
        mining()


def sound():
    cls()
    print(Fore.LIGHTCYAN_EX + "Настройка музыки:\n")
    print(Fore.LIGHTMAGENTA_EX + "Эпическая - EPIC")
    print(Fore.RED + "Выключить музыку - STOP")
    print(Fore.LIGHTRED_EX + "Вернуться - RETURN")
    command = input(Fore.LIGHTRED_EX + "Введите команду: ")
    if command.upper() in ["EPIC", "1"]:
        enableEpicMusic()
        sound()
    elif command.upper() in ["STOP", "2"]:
        disableMusic()
        sound()
    elif command.upper() in ["RETURN", "3"]:
        menu()
    else:
        cls()
        colored_printing("Команда неверная! Повторите ввод.", 0.05, Fore.RED, False)
        sleep(1)
        sound()


def rand_event_1():
    global Balance
    cls()
    disableMusic()
    colored_printing("Пока ты шёл домой с магазина 'ЭНВИДЕ', тебя отпиздили гопники из соседнего подъезда.", 0.05,
                     Fore.LIGHTWHITE_EX, False)
    sleep(1)
    Balance -= 10000
    colored_printing("-10000 рублей", 0.08, Fore.LIGHTRED_EX, False)
    sleep(1.2)
    enableEpicMusic()
    mining()


def rand_event_2():
    global BlitCoin
    cls()
    disableMusic()
    colored_printing("Просматривая ютубчег, ты заметил что твои фермы неожиданно перезагрузились.", 0.05,
                     Fore.LIGHTCYAN_EX, False)
    sleep(1)
    colored_printing("Ты не придал этому значения, и продолжил смотреть ютубчег. Так прошло пару дней.", 0.05,
                     Fore.LIGHTWHITE_EX, False)
    sleep(1)
    colored_printing(
        "Но потом, когда ты захотел снять крипту, ты обнаружил что твои фермы заражены вирусом NJrat и работают на хакеров.",
        0.05, Fore.LIGHTRED_EX, False)
    sleep(1)
    BlitCoin -= 0.01
    colored_printing("-0.01 блиткоинов", 0.08, Fore.RED, False)
    sleep(1.2)
    enableEpicMusic()
    mining()


def final():
    cls()
    disableMusic()
    sleep(1)
    colored_printing("Ты был очень счастлив, когда купил новый прототип фермы BJ Infinity React.", 0.1, Fore.GREEN,
                     False)
    sleep(0.5)
    colored_printing("Запустив ферму, ты чуть не закричал на всю хату - ЕБАТЬ КАК МНОГО БЛИТКОВ, Я БОГАТ!!!!", 0.1,
                     Fore.LIGHTMAGENTA_EX, False)
    sleep(0.5)
    colored_printing(
        "Но через несколько дней счастья, ты с удивлением обнаружил, что в новостях все писали, что блиткоин потребляет энергии как целая страна!",
        0.1, Fore.LIGHTRED_EX, False)
    sleep(0.5)
    colored_printing(
        "Ты искренне не хотел верить, что это из-за твоей фермы. Но однажды... В дверь твоей хаты постучали...", 0.15,
        Fore.GREEN, False)
    sleep(2)
    copyrights()


def copyrights():
    cls()
    enableFinal()
    colored_printing("Режиссёр - BlitGaming", 0.1, Fore.LIGHTGREEN_EX, False)
    colored_printing("Программист - BlitGaming", 0.1, Fore.LIGHTMAGENTA_EX, False)
    colored_printing("Ну и просто хороший чувак - BlitGaming", 0.1, Fore.LIGHTBLUE_EX, False)
    sleep(0.75)
    colored_printing("Спасибо что играл в мою игру, надеюсь тебе понравилось :). Я старался)", 0.1, Fore.LIGHTCYAN_EX, False)
    sleep(125)


if __name__ == "__main__":
    main()
