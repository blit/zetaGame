import random
import os
from colorama import Fore
from time import sleep
from pygame import init as pg_init
from pygame import mixer
from dict import *


def cls():
    os.system("cls" if os.name == "nt" else "clear")

def special_cls():
    print("")
    cls()


pg_init()
cls()
music = mixer.music


def enableSilentMusic():
    music.load("silent.mp3")
    music.play(loops=-1, fade_ms=500)


def enableEpicMusic():
    music.load("epic.mp3")
    music.play(loops=-1, fade_ms=500)

def enableFinal():
    music.load("final.mp3")
    music.play(fade_ms=1000)

def startup():
    music.load("startup.mp3")
    music.play(fade_ms=500)


def disableMusic():
    music.stop()


def printing(text, speed):
    text = "\n" + text
    for element in text:
        sleep(speed)
        print(element, end="", flush=True)


def colored_printing(text, speed, color, breakly):
    text = "\n" + text
    print(color, end="", flush=True)
    if not breakly:
        for element in text:
            sleep(speed)
            print(element, end="", flush=True)
    elif breakly:
        for element in text:
            sleep(speed)
            print(element, end="", flush=True)
        print("")


def loading(speed, color):
    ss = "█"
    colored_printing("ZetaGame One", 0.08, Fore.LIGHTRED_EX, False)
    sleep(2)
    startup()
    for i in range(101):
        sleep(speed)
        print(f"{color}\rЗагрузка", str(i), "%", i * ss, end="")
    sleep(0.75)
    cls()


def mine(Asic):
    ss = "█"
    for i in range(101):
        sleep(0.1)
        print(Fore.LIGHTGREEN_EX + "\rМайним блитки...", str(i), "%", i * ss, end="")
    cash = float(random.randint(1, 100000000000))
    cash = round((cash / 100000000000) * Minec[Asic], 8)
    print(Fore.LIGHTYELLOW_EX + f"\nПолучено: {cash} блиткоинов.")
    return cash


def trade(BlitCoins, Course):
    ss = "█"
    for i in range(101):
        sleep(0.05)
        print(Fore.LIGHTYELLOW_EX + "\rТрейдимся...", str(i), "%", i * ss, end="")
    bal = round(Course * BlitCoins, 2)
    print(Fore.LIGHTYELLOW_EX + f"\nПолучено: {bal} рублей.")
    return bal
